{ stdenv, lib, pkgs ? import <nixpkgs> {}, ... }: pkgs.rustPlatform.buildRustPackage {
  pname = "nexusfetch";
  version = "git";

  src = ./.;

  nativeBuildInputs = lib.optionals stdenv.isLinux (with pkgs; [
    pkg-config
  ]);

  propagatedBuildInputs = lib.optionals stdenv.isLinux (with pkgs; [
    clang
    rpm
  ]);

  cargoLock = {
    lockFile = ./Cargo.lock;
  };

  meta = {
    description = "Small fetch written in rust";
    license = lib.licenses.gpl3;
    maintainers = [ lib.maintainers.alxhr0 ];
    mainProgram = "nexusfetch";
  };
}

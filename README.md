# nexusfetch

Small fetch written in Rust

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

## Showcase
![Nexusfetch showcase](./assets/image.png)

## Installation
```bash
git clone https://gitlab.com/alxhr0/nexusfetch.git
cd nexusfetch
cargo build --release # or cargo install --path . if you want to install it
```

### From Crates.io

``` bash
cargo install nexusfetch
``` 

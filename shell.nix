let
  pkgs = import <nixpkgs> { };
in
pkgs.mkShell {

  RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
  
  packages = with pkgs; [
    pkg-config
    libselinux
    rustc
    cargo
    rust-analyzer
    rustfmt
    linuxHeaders
    llvmPackages_18.clang-unwrapped
  ];

  LD_LIBRARY_PATH =
    with pkgs;
    pkgs.lib.makeLibraryPath [
      rpm
      libselinux
      llvmPackages_18.clang-unwrapped
      linuxHeaders
    ];
}

{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... } @inputs: flake-utils.lib.eachDefaultSystem(system: let
    pkgs = import nixpkgs {inherit system overlays;};

    # same as the one in the overlay but with the current pkgs iteration (current system).
    package = pkgs.callPackage ./default.nix {};

    overlays = [mainOverlay];

    mainOverlay = (final: _: {
      nexusfetch = final.callPackage ./default.nix {};
    });
  in {
    packages.default = package;
    overlays.default = mainOverlay;

    devShells.default = let inherit (pkgs) mkShell; in mkShell {
      name = "dev";
      nativeBuildInputs = with pkgs; [ pkg-config ];
      propagatedBuildInputs = with pkgs; [ clang rpm ];
      buildInputs = with pkgs; [ package cargo rustc ];
    };
  });
}

#![allow(dead_code)]

use clap::Parser;

#[derive(Parser)]
#[command(propagate_version = true)]
pub struct Cli {
    #[clap(short, long)]
    /// Show version
    pub version: bool,
    #[clap(short, long)]
    pub info: bool,
    // Show info
    #[clap(short, long)]
    pub os: Option<String>,
}

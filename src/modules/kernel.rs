use colored::*;
use std::path::Path;
use std::process::Command;

pub fn getkernel() {
    if Path::new("C:/Windows").exists() {
        #[cfg(windows)]
        {
            use winver::WindowsVersion;
            let version = WindowsVersion::detect().unwrap();
            print!(
                "{}{} {}\n",
                crate::ASCII[3],
                "Kernel: NT ".bold().blue(),
                version
            );
        }
    } else {
        let mut kernelget = Command::new("uname");
        kernelget.arg("-r");

        let kernel_version = String::from_utf8(kernelget.output().unwrap().stdout).unwrap();
        print!(
            "{}{} {}\n",
            crate::ASCII[3].blue(),
            "Kernel:".bold().blue(),
            kernel_version.trim()
        );
    }
}

use colored::*;
use std::path::Path;

pub fn getinit() {
    if Path::new("/bin/freebsd-version").exists() {
        println!("{}{} RC", crate::ASCII[6].blue(), "Init:".blue().bold());
    } else if Path::new("/usr/bin/systemctl").exists() {
        println!(
            "{}{} Systemd",
            crate::ASCII[6].blue(),
            "Init:".blue().bold()
        );
    } else if Path::new("/run/current-system/sw/bin/systemctl").exists() {
        println!(
            "{}{} Systemd",
            crate::ASCII[6].blue(),
            "Init:".blue().bold()
        );
    } else if Path::new("/usr/sbin/runit").exists() {
        println!("{}{} Runit", crate::ASCII[6].blue(), "Init:".blue().bold());
    } else if Path::new("/usr/bin/openrc").exists() {
        println!("{}{} openRC", crate::ASCII[6].blue(), "Init:".blue().bold());
    }
}

use colored::*;
use libmacchina::{traits::GeneralReadout as _, GeneralReadout};

pub fn getuserhost() {
    let general_readout = GeneralReadout::new();
    print!(
        "{}{}@{} \n",
        crate::ASCII[0].blue(),
        general_readout.username().unwrap().bold().blue(),
        general_readout.hostname().unwrap().bold().blue()
    );
}

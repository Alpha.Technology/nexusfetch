use colored::*;
use regex::Regex;
use std::env;
use std::path::Path;
use std::process::Command;

fn run_cmd_unsafe(cmd: &str) -> String {
    let output = Command::new("sh")
        .args(["-c", cmd])
        .output()
        .unwrap()
        .stdout;
    String::from_utf8(output).unwrap().trim().to_string()
}

pub fn getshell() {
    if Path::new("C:/Windows").exists() {
        print!("{} PowerShell\n", "Shell:".bold().blue());
    } else {
        let ver_regex = Regex::new(r"(0|[1-9]\d*)\.(0|[1-9]\d*)\.?(0|[1-9]\d*)?(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?").unwrap();
        let find_shell = env::var("SHELL").unwrap();

        if Path::new(&format!("/usr/local/bin")).exists()
            && Path::new("/bin/freebsd-version").exists()
        {
            let shell = find_shell.replace("/usr/local/bin/", "");

            let version = run_cmd_unsafe(format!("{shell} --version").as_str());
            let locations = ver_regex.find(&version).unwrap();
            let version = &version[locations.start()..locations.end()];
            print!(
                "{}{} {shell} {version}\n",
                crate::ASCII[4].blue(),
                "Shell:".bold().blue()
            );
        } else if Path::new("/run/current-system/sw").exists() {
            let shell = find_shell.replace("/run/current-system/sw/bin/", "");

            if !Path::new(&format!("/run/current-system/sw/bin/{shell}")).exists() {
                print!(
                    "{}{} {shell}\n",
                    crate::ASCII[4].blue(),
                    "Shell:".bold().blue()
                );
            } else if shell == "dash" {
                print!(
                    "{}{} Dash\n",
                    crate::ASCII[4].blue(),
                    "Shell:".bold().blue()
                );
            } else {
                let version = run_cmd_unsafe(format!("{shell} --version").as_str());
                let locations = ver_regex.find(&version).unwrap();
                let version = &version[locations.start()..locations.end()];
                print!(
                    "{}{} {shell} {version}\n",
                    crate::ASCII[4].blue(),
                    "Shell:".bold().blue()
                );
            }
        } else if Path::new("/usr/bin").exists() {
            let shell = find_shell.replace("/usr/bin/", "");

            if !Path::new(&format!("/usr/bin/{shell}")).exists() {
                print!(
                    "{}{} {shell}\n",
                    crate::ASCII[4].blue(),
                    "Shell:".bold().blue()
                );
            } else if shell == "dash" {
                print!(
                    "{}{} Dash\n",
                    crate::ASCII[4].blue(),
                    "Shell:".bold().blue()
                );
            } else {
                let version = run_cmd_unsafe(format!("{shell} --version").as_str());
                let locations = ver_regex.find(&version).unwrap();
                let version = &version[locations.start()..locations.end()];
                print!(
                    "{}{} {shell} {version}\n",
                    crate::ASCII[4].blue(),
                    "Shell:".bold().blue()
                );
            }
        } else {
            let shell = find_shell.replace("/bin/", "");

            if !Path::new(&format!("/bin/{shell}")).exists() {
                print!(
                    "{}{} {shell}\n",
                    crate::ASCII[4].blue(),
                    "Shell:".bold().blue()
                );
            } else if shell == "dash" {
                print!(
                    "{}{} Dash\n",
                    crate::ASCII[4].blue(),
                    "Shell:".bold().blue()
                );
            } else {
                let version = run_cmd_unsafe(format!("{shell} --version").as_str());
                let locations = ver_regex.find(&version).unwrap();
                let version = &version[locations.start()..locations.end()];
                print!(
                    "{}{} {shell} {version}\n",
                    crate::ASCII[4].blue(),
                    "Shell:".bold().blue()
                );
            }
        }
    }
}

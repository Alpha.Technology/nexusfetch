use colored::*;
use libmacchina::{traits::GeneralReadout as _, GeneralReadout};
use std::process::Command;
use regex::Regex;

fn run_cmd_unsafe(cmd: &str) -> String {
    let output = Command::new("bash")
        .args(["-c", cmd])
        .output()
        .unwrap()
        .stdout;
    String::from_utf8(output).unwrap().trim().to_string()
}

pub fn getterminalname() {
    let ver_regex = Regex::new(r"(0|[1-9]\d*)\.(0|[1-9]\d*)\.?(0|[1-9]\d*)?(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?").unwrap();
    let general_readout = GeneralReadout::new();

    let terminal_name = general_readout.terminal().unwrap_or("None Detected".to_string()).trim().to_string();

    match terminal_name.as_str() {
        "st" => {
            let terminal_version = "";

            print!(
                "{}{} {terminal_name} {terminal_version}\n",
                crate::ASCII[6].white(),
                "Term:".bold().blue(),
            );
        },

        "gnome-terminal-" => {
            let terminal_name = "gnome-terminal";
            let terminal_version = run_cmd_unsafe(format!("{terminal_name} --version").as_str());
            let locations = ver_regex.find(&terminal_version).unwrap();
            let version = &terminal_version[locations.start()..locations.end()];
        
            print!(
                "{}{} {terminal_name} {version}\n",
                crate::ASCII[6].white(),
                "Term:".bold().blue(),
            );
        },
        
        "wezterm-gui" => {
            let terminal_name = "wezterm";
            let terminal_version = run_cmd_unsafe(format!("{terminal_name} --version").as_str());
            let locations = ver_regex.find(&terminal_version).unwrap();
            let version = &terminal_version[locations.start()..locations.end()];
        
            print!(
                "{}{} {terminal_name} {version}\n",
                crate::ASCII[6].white(),
                "Term:".bold().blue(),
            );
        },

        "sakura" => {
            let terminal_name = "sakura";
            let terminal_version = run_cmd_unsafe(format!("{terminal_name} --version").as_str());
            let locations = ver_regex.find(&terminal_version).unwrap();
            let version = &terminal_version[locations.start()..locations.end()];

            print!(
                "{}{} {terminal_name} {version}\n",
                crate::ASCII[6].white(),
                "Term:".bold().blue(),
            );
        },

        _ => {
            let terminal_version = run_cmd_unsafe(format!("{terminal_name} --version").as_str());
                    
            if terminal_version.is_empty() {
                print!(
                    "{}{} {terminal_name}\n",
                    crate::ASCII[6].white(),
                    "Term:".bold().blue(),
                );
            } else {

                let locations = ver_regex.find(&terminal_version).unwrap();
                let version = &terminal_version[locations.start()..locations.end()];

                print!(
                    "{}{} {terminal_name} {version}\n",
                    crate::ASCII[6].white(),
                    "Term:".bold().blue(),
                );
            }
        

        }
    }

}

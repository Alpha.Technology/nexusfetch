use colored::*;
use libmacchina::{traits::GeneralReadout as _, GeneralReadout};

pub fn getuptime() {
    let general_readout = GeneralReadout::new();
    let uptime = general_readout.uptime().unwrap();

    let days = if uptime > 86400 {
        let days_pre = uptime / 60 / 60 / 24;
        days_pre.to_string() + "d "
    } else {
        "".to_string()
    };
    let hours = if uptime > 3600 {
        let hours_pre = (uptime / 60 / 60) % 24;
        hours_pre.to_string() + "h "
    } else {
        "".to_string()
    };
    let minutes = if uptime > 60 {
        let minutes_pre = (uptime / 60) % 60;
        minutes_pre.to_string() + "m"
    } else {
        "0m".to_string()
    };

    print!(
        "{}{} {}{}{}\n",
        crate::ASCII[5].blue(),
        "Uptime:".bold().blue(),
        days,
        hours,
        minutes
    );
}

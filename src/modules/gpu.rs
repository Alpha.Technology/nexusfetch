use colored::*;
use libmacchina::{traits::GeneralReadout as _, GeneralReadout};
use pciid_parser::Database;
use std::path::Path;

pub fn getgpu() {
    if Path::new("/run/current-system").exists() {
        if Database::read().is_err() {}

        return;
    } else {
        if Database::read().is_err() {
            return;
        }

        let general_readout = GeneralReadout::new();
        let gpu = general_readout.gpus();

        if gpu.is_err() {
            return;
        }

        // it is safe now
        let gpu = gpu.unwrap();

        print!(
            "{}{} {}\n",
            crate::ASCII[6].blue(),
            "GPU:".bold().blue(),
            gpu.join(&format!(
                "\n{}{} ",
                crate::ASCII[6].blue(),
                "GPU2:".bold().blue()
            ))
        );
    }
}

use colored::*;
use dotenvy;
use regex::Regex;
use std::path::Path;
use clap::*;

pub fn getos() {

    let args = crate::cli::Cli::parse();

    if let Some(s) = args.os {
        print!(
            "{}{} {}\n",
            crate::ASCII[1].blue(),
            "OS:".bold().blue(),
            s
        );
        return;
    }

    if Path::new("C:/Windows").exists() {
        #[cfg(windows)]
        {
            use winver::WindowsVersion;
            let version = WindowsVersion::detect().unwrap();
            print!(
                "{}{} Windows {}\n",
                crate::ASCII[1].blue(),
                "OS:".bold().blue(),
                version
            );
        }
    } else if dotenvy::var("PATH")
        .unwrap_or_default()
        .contains("/bedrock/cross/bin")
    {
        let x = std::fs::read_to_string("/bedrock/etc/os-release").unwrap();
        let re = Regex::new(r#"(?m)^PRETTY_NAME=(?:(?:"(.*?)")|(?:(.*)))$"#).unwrap();
        let y = re.captures(&x).unwrap();
        print!(
            "{}{} {}\n",
            crate::ASCII[1].blue(),
            "OS:".bold().blue(),
            y.get(2).or(y.get(1)).unwrap().as_str()
        );
    } else if Path::new("/etc/os-release").exists() {
        let x = std::fs::read_to_string("/etc/os-release").unwrap();
        let re = Regex::new(r#"(?m)^PRETTY_NAME=(?:(?:"(.*?)")|(?:(.*)))$"#).unwrap();
        let y = re.captures(&x).unwrap();
        print!(
            "{}{} {}\n",
            crate::ASCII[1].blue(),
            "OS:".bold().blue(),
            y.get(2).or(y.get(1)).unwrap().as_str()
        );
    }
}

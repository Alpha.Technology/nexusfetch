use colored::*;
use libmacchina::{traits::MemoryReadout as _, MemoryReadout};

pub fn getmem() {
    let memory_readout = MemoryReadout::new();

    let total_mem = memory_readout.total().unwrap();
    let used_mem = memory_readout.used().unwrap();

    let total_mem2 = total_mem / 1024;
    let used_mem2 = used_mem / 1024;

    print!(
        "{}{} {}M / {}M\n",
        crate::ASCII[6].blue(),
        "Mem:".bold().blue(),
        used_mem2,
        total_mem2
    );
}

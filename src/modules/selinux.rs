use colored::Colorize;
use std::path::Path;

#[cfg(feature = "with-selinux")]
pub fn getselinux() {
    if Path::new("/sys/fs/selinux").exists() {
        use selinux::*;
        let current_status = current_mode();
        println!("{}{}: {:?}", crate::ASCII[6].blue() ,"SELinux".bold().blue(), current_status);
    }
}

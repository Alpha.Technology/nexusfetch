use colored::*;

pub fn printcolors() {
    print!(
        "{}{} {} {} {} {} {} {} {}",
        crate::ASCII[6].blue(),
        " ".black(),
        " ".red(),
        " ".green(),
        " ".yellow(),
        " ".blue(),
        " ".magenta(),
        " ".cyan(),
        " ".white()
    );
}

use colored::*;
use libmacchina::{traits::GeneralReadout as _, GeneralReadout};
use std::process::Command;

fn set_xdg_session_type() -> Result<(), String> {
    let output_str = match get_command_output_as_string() {
        Ok(output) => output,
        Err(err) => return Err(err.to_string()),
    };
    let terminal = std::env::var("TERM").unwrap_or("linux".into());

    if output_str.trim() == "wayland" && terminal != "linux" {
        std::env::set_var("XDG_SESSION_TYPE", "wayland");
    } else if output_str.trim() == "x11" && terminal != "linux" {
        std::env::set_var("XDG_SESSION_TYPE", "x11");
    }

    Ok(())
}

fn find_wm() -> String {
    let general_readout = GeneralReadout::new();

    let _ = set_xdg_session_type();

    return general_readout.window_manager().unwrap_or("unknown".into());
}

fn get_command_output_as_string() -> Result<String, String> {
    let mut check_session_type = Command::new("bash");
    let session_type_command = check_session_type.arg("loginctl show-session $(awk '/tty/ {print $1}' <(loginctl)) -p Type | awk -F= '{print $2}'").output().unwrap();

    let output_str = String::from_utf8(session_type_command.stdout).map_err(|e| e.to_string())?;
    Ok(output_str)
}

pub fn getwm() {
    let wm = find_wm();

    if wm == "unknown" {
        return;
    }

    print!("{}{} {}\n", crate::ASCII[6].blue(), "WM:".bold().blue(), wm);
}
